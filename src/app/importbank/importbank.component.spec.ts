import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportbankComponent } from './importbank.component';

describe('ImportbankComponent', () => {
  let component: ImportbankComponent;
  let fixture: ComponentFixture<ImportbankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportbankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportbankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
